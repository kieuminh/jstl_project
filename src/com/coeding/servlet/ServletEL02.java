package com.coeding.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.vo.BloggerVO;

@WebServlet("/el02")
public class ServletEL02 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServletEL02() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO storage in webapp
		// .getAttribute();
		List<BloggerVO> list = new ArrayList<BloggerVO>();
		for(int i=0; i<5; i+=1) {
			BloggerVO vo = new BloggerVO();
			vo.setName("bloger 0"+i);
			list.add(vo);
		}
		System.out.println("Nothingtosay");
		request.setAttribute("list", list);// ServletRequest
		// other JSP
		RequestDispatcher rd = 
				request.getRequestDispatcher("/jsp/jstl02.jsp");
		rd.forward(request, response);
		
		
	}

}
