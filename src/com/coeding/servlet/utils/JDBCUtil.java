package com.coeding.servlet.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/*
 *  our goal !! use DB
 *  1. add JDBC library for your DBMS(mysql)
 *  	+ mysql-connector-java
 *  	+ /WEB-INF/lib/~.jar on Dynamic Web Project
 *  	+ "Web App Libraries"
 *  2. make Connection DB
 *  	+ create instance of connector driver
 *  	+ login to DB with url, id ,passwd
 *  3. send SQL to DB
 *  4. handling result of DB 
 *  
 */
public abstract class JDBCUtil {
	protected Connection conn;
	private DataSource factory;
	public JDBCUtil() {
		// JNDI
		try {
			Context ctx = new InitialContext();
			Context env = (Context)ctx.lookup("java:/comp/env");
			factory = (DataSource)env.lookup("jdbc/mysql");
			// return DataSource
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
/*	
	private Connection getConnectionBasic() {
		Connection conn = null;
		// hard coding <- difficult maintain
		String driver = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/demo";
		String username = "root";
		String passwd = "";
		try {
			// 1. create instance of Driver
			Class.forName(driver);
			// 2. login
			conn = DriverManager.getConnection(url, username, passwd);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return conn;		
	}
	private Connection getConnectionFile() {
		Connection conn = null;
		String resource = "C:\\Users\\Administrator\\git\\webservlet\\WebContent\\WEB-INF\\config\\app.properties";
		File file = new File(resource);
		Properties props = new Properties();
		try {
			props.load(new FileReader(file));
			String driver = props.getProperty("jdbc.driver");
			String url = props.getProperty("jdbc.url");
			String username = props.getProperty("jdbc.username");
			String passwd = props.getProperty("jdbc.password");
			// 1. create instance of Driver
			Class.forName(driver);
			// 2. login
			conn = DriverManager.getConnection(url, username, passwd);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return conn;		
	}
*/
	// TemplateMethod pattern
	final public Object transaction() {
		System.out.println("connect DB");
		try {
			// 1. get connection
			conn = factory.getConnection();
			// 2. sql request, handling response
			return process();// business logic
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 3. close
			try {
				conn.close();
				System.out.println("closed DB connection");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	// i dont know what you want to do
	protected abstract Object process() throws SQLException;
	// implement class child extends abstract class as parent
	// down-casting
	
}
