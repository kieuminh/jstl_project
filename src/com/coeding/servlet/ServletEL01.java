package com.coeding.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.vo.BloggerVO;

@WebServlet("/el01")
public class ServletEL01 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServletEL01() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO storage in webapp
		// .getAttribute();
		List<BloggerVO> list = new ArrayList<BloggerVO>();
		request.setAttribute("blogerList", list);

		BloggerVO vo1 = new BloggerVO();
		vo1.setName("ServletRequest");
		request.setAttribute("bloger1", vo1);
		
		BloggerVO vo2 = new BloggerVO();
		vo2.setName("HttpSession");
		request.getSession().setAttribute("bloger2", vo2);
		
		BloggerVO vo3 = new BloggerVO();
		vo3.setName("ServletContext");
		request.getServletContext().setAttribute("bloger3", vo3);
		
		// other JSP
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/recvAttr.jsp");
		rd.forward(request, response);
		
	}

}
