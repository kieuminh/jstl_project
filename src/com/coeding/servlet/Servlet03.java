package com.coeding.servlet;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class Servlet03 extends GenericServlet {
	private static final long serialVersionUID = -7030058806903027925L;

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {
		System.out.println(this.getClass().getName()+" => service");
		String info = getServletInfo();
		ServletConfig config = getServletConfig();
		System.out.println(info + ": "+config);
	}

}
