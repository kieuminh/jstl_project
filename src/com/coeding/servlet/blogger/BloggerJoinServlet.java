package com.coeding.servlet.blogger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.dao.BloggerDAO;
import com.coeding.servlet.vo.BloggerVO;

@WebServlet("/joinServlet")
public class BloggerJoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private BloggerDAO dao;
    public BloggerJoinServlet() {
		
        super();
		dao=new BloggerDAO();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("joinServlet");
		
		// get form data		
		String name = request.getParameter("uname");
		String email = request.getParameter("uemail");
		String passwd = request.getParameter("upass");
		// next
		BloggerVO blogger=new BloggerVO();
		blogger.setName(name);
		blogger.setEmail(email);
		blogger.setPasswd(passwd);
	
		dao.insert(blogger);
		response.sendRedirect("/webservlet/blogger");
	}

}
