package com.coeding.servlet.blogger;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.dao.BloggerDAO;
import com.coeding.servlet.vo.BloggerVO;

@WebServlet("/blogger")
public class BloggerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BloggerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		// TODO: send data to client
		BloggerDAO dao = new BloggerDAO();
		// list of Blogger
		List<BloggerVO> list = dao.listBlogger(); 
		request.setAttribute("userlist", list);
		// forwarding to view rendering Client move
//		response.sendRedirect("jsp/blogger/list.jsp");// fill URL
		
		// dispatching Server side move
		RequestDispatcher dis = request.getRequestDispatcher("jsp/blogger/list.jsp");
		dis.forward(request, response);
		// dead code
//		
//		String html = "<!DOCTYPE html><html><body>";
//		html += "<table id='userTable'>";
//		for(int i=0; i<list.size(); i+=1){
//			BloggerVO user = list.get(i);// new Blogger(); <-- error ?
//			html += "<tr>";
//			html += "<td>"+user.getUid()+"</td>";
//			html += "<td>"+user.getName()+"</td>";
//			html += "<td>"+user.getEmail()+"</td>";
//			html += "</tr>";
//		}
//		html += "</table>";
//		html += "</body></html>";// khong dep, i'm not artist
//		
//		// send with stream to reponse
//		response.setContentType("text/html;charset=utf-8");
//		PrintWriter out = response.getWriter();
//		out.print(html);
//		out.flush();
//		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO: save data from client
		doGet(request, response);
	}

}
