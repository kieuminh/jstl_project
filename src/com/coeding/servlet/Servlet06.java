package com.coeding.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/upload")
@MultipartConfig
public class Servlet06 extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Servlet06() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO save file into server
		Collection<Part> parts = request.getParts(); // from client(browser)
		String fileName="";
		ServletContext ctx = request.getServletContext();
		String uploadpath = ctx.getRealPath(ctx.getInitParameter("uploadDir"));
		System.out.println(uploadpath);
		for(Part part : parts) {
			if( part.getContentType() != null ) {
				// has file data
				String name = part.getName();// <input type="file" name="">
				System.out.println(name);
				fileName = part.getSubmittedFileName();// upload file's name
				System.out.println(fileName);
				// save into server path have to be realPath
				String filepath = uploadpath+File.separator+fileName;
				part.write(filepath);
			}
		}
		// send response
		// ServletContext shared data all Servlet , JSP
		request.getServletContext().setAttribute("filepath",
				request.getContextPath()+"/uploads/"+fileName);
		// notice!! is not used Dispatcher
		response.sendRedirect(request.getContextPath()+"/jsp/form-file.jsp");
	}

}
