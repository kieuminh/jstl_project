package com.coeding.servlet.article;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.dao.ArticleDAO;
import com.coeding.servlet.vo.ArticleVO;

@WebServlet("/article/new")
public class ArticleNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ArticleNewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO send form dont change URL
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/article/form-new.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO save to DB getAttribute(); from other servlet
		String title = request.getParameter("title");//<-- from client
		String name = request.getParameter("name");
		String content = request.getParameter("content");
		ArticleVO article = new ArticleVO();
		article.setTitle(title);
		article.setName(name);
		article.setContent(content);
		
		ArticleDAO dao = new ArticleDAO();
		dao.insert(article);
		// changed by client
		response.sendRedirect("/webservlet/article/list");
	}

}
