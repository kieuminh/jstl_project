package com.coeding.servlet.article;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.dao.ArticleDAO;
import com.coeding.servlet.vo.ArticleVO;

@WebServlet("/article/view")
public class ArticleDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ArticleDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		long aid = Long.parseLong(id);
		ArticleDAO dao = new ArticleDAO();
		ArticleVO article = dao.selectOne(aid);
		request.setAttribute("article", article);
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/article/detail.jsp");
		rd.forward(request, response);
	}

}
