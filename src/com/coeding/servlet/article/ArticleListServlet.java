package com.coeding.servlet.article;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.dao.ArticleDAO;
import com.coeding.servlet.vo.ArticleVO;

@WebServlet("/article/list")
public class ArticleListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ArticleListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub 		response.getWriter().append("Served at: ").append(request.getContextPath());
		ArticleDAO dao = new ArticleDAO();
		List<ArticleVO> list = dao.listArticle();
		// send list to servlet or jsp
		request.setAttribute("articleList", list);
		RequestDispatcher rd = request.getRequestDispatcher(
				"/jsp/article/list.jsp");
		rd.forward(request, response);	
	}

}
