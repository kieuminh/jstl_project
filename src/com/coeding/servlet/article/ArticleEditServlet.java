package com.coeding.servlet.article;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coeding.servlet.dao.ArticleDAO;
import com.coeding.servlet.vo.ArticleVO;

/**
 * Servlet implementation class ArticleEditServlet
 */
@WebServlet("/article/edit")
public class ArticleEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArticleEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		ArticleDAO articledao = new ArticleDAO();// parse
		ArticleVO article = articledao.selectOne(Long.parseLong(id));//<<<<
		request.setAttribute("Article", article);
		RequestDispatcher rd = request.getRequestDispatcher("/jsp/article/form-edit.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");// form input name
		String title = request.getParameter("title");
		String name = request.getParameter("name");
		String content = request.getParameter("content");
		ArticleVO article = new ArticleVO();
		article.setAid(Long.parseLong(id));
		article.setTitle(title);
		article.setName(name);
		article.setContent(content);
		
		ArticleDAO articledao = new ArticleDAO();// parse
		articledao.update(article);
		response.sendRedirect("/webservlet/article/view?id="+article.getAid());	
	}

}
