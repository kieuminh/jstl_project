package com.coeding.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class Servlet02 implements Servlet {
	private ServletConfig config;
	@Override
	public void destroy() {
		System.out.println(this.getClass().getName()+" => destroy");
	}

	@Override
	public ServletConfig getServletConfig() {
		System.out.println(this.getClass().getName()+" => get servlet config");
		// from Container
		return config;
	}

	@Override
	public String getServletInfo() {
		System.out.println(this.getClass().getName()+" => get servlet info");
		return "author: jang, version:1.0.0, company:upup";
	}

	@Override
	public void init(ServletConfig arg0) throws ServletException {
		System.out.println(this.getClass().getName()+" => init");
		config = arg0;
		// get <init-param> value at web.xml
		String name = config.getInitParameter("author");
		System.out.println(name);
		ServletContext ctx = config.getServletContext();
		String version = ctx.getInitParameter("version");
		System.out.println(version);
	}

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {
		System.out.println(this.getClass().getName()+" => service");
		String info = getServletInfo();
		ServletConfig config = getServletConfig();
		System.out.println(info + ": "+config);
	}

}
