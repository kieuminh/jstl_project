package com.coeding.servlet.vo;

import java.util.Date;

public class BloggerVO {
	private Long uid;
	private String name;
	private String email;
	private String passwd;
	private Date joinDate;
	
	public long getUid() {
		return uid;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BloggerVO(long uid, String name, String email, String passwd, Date joinDate) {
		this.uid = uid;
		this.name = name;
		this.email = email;
		this.passwd = passwd;
		this.joinDate = joinDate;
	}
	// default constructor
	public BloggerVO() {}
	
	

}
