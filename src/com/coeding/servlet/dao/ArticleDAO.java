package com.coeding.servlet.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.coeding.servlet.utils.JDBCUtil;
import com.coeding.servlet.vo.ArticleVO;

public class ArticleDAO {
	public void update(ArticleVO vo) {
		new JDBCUtil(){
			@Override
			protected Object process() throws SQLException {
				String sql="update from tbl_article"
						+" set title=?, content=?, name=? where aid=?";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getTitle());
				pstmt.setString(2, vo.getContent());
				pstmt.setString(3, vo.getName());
				pstmt.setLong(4, vo.getAid());
				pstmt.executeUpdate();		
				pstmt.close();
				return null;
			}			
		}.transaction();
	}
	public void insert(ArticleVO vo) {
		new JDBCUtil(){
			@Override
			protected Object process() throws SQLException {
				String sql="insert into tbl_article"
						+" (title, content, name) values (?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getTitle());
				pstmt.setString(2, vo.getContent());
				pstmt.setString(3, vo.getName());
				pstmt.executeUpdate();				
				pstmt.close();
				return null;
			}			
		}.transaction();
	}
	public List<ArticleVO> listArticle() {
		Object obj = new JDBCUtil() {
			@Override
			protected Object process() throws SQLException {
				String sql = "select * from tbl_article";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				List<ArticleVO> list = new ArrayList<ArticleVO>();
				while(rs.next()) {
					long id = rs.getLong("aid");
					String title = rs.getString("title");
					String content = rs.getString("content");
					String name = rs.getString("name");
					Date wdate = rs.getDate("wdate");
					
					ArticleVO article = new ArticleVO();
					article.setAid(id);
					article.setTitle(title);
					article.setName(name);
					article.setContent(content);
					article.setWdate(wdate);
					list.add(article);
				}
				rs.close();
				stmt.close();
				return list;
			}
		}.transaction();
		List<ArticleVO> list = (ArrayList<ArticleVO>)obj;
		return list;
	}
	public ArticleVO selectOne(long aid) {
		Object obj = new JDBCUtil() {
			@Override
			protected Object process() throws SQLException {
				String sql = "select * from tbl_article where aid="+aid;
				PreparedStatement pstmt = conn.prepareStatement(sql);
				ResultSet rs = pstmt.executeQuery();
				ArticleVO vo = new ArticleVO();
				while(rs.next()) {
					vo.setAid(rs.getLong("aid"));
					vo.setTitle(rs.getString("title"));
					vo.setName(rs.getString("name"));
					vo.setContent(rs.getString("content"));
					vo.setWdate(rs.getDate("wdate"));
				}
				return vo;
			}
			
		}.transaction();
		return (ArticleVO)obj;
	}


}
