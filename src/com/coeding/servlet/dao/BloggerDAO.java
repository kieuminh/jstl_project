package com.coeding.servlet.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.coeding.servlet.utils.JDBCUtil;
import com.coeding.servlet.vo.BloggerVO;

public class BloggerDAO {
	public void insert(BloggerVO vo) {
		new JDBCUtil(){

			@Override
			protected Object process() throws SQLException {
				String sql="insert into tbl_user (name, email, passwd) values (?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getName());
				pstmt.setString(2, vo.getEmail());
				pstmt.setString(3, vo.getPasswd());
				pstmt.executeUpdate();
				
				return null;
			}
			
		}.transaction();
	}
	public List<BloggerVO> listBlogger() {
		Object obj = new JDBCUtil() {
			@Override
			protected Object process() throws SQLException {
				String sql = "select * from tbl_user";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);
				List<BloggerVO> list = new ArrayList<BloggerVO>();
				while(rs.next()) {
					long id = rs.getLong("uid");
					String name = rs.getString("name");
					String email = rs.getString("email");
					BloggerVO user = new BloggerVO();
					user.setUid(id);
					user.setName(name);
					user.setEmail(email);
					list.add(user);
				}
				rs.close();
				stmt.close();
				return list;
			}
		}.transaction();
		List<BloggerVO> list = (ArrayList<BloggerVO>)obj;
		return list;
	}

}
