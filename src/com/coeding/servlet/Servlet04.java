package com.coeding.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/step04")
public class Servlet04 extends HttpServlet {
	private static final long serialVersionUID = -8421234290153770251L;

//	@Override
//	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		System.out.println(arg0.getMethod());
//		if(arg0.getMethod().equals("GET")) {
//			doGet(arg0, arg1);
//		}
//		if(arg0.getMethod().equals("POST")) {
//			doPost(arg0, arg1);
//		}
//	}
//
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("GET");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("POST");
	}
	
	// support RESTful application doPut, doDelete
	// front - API - back
}
