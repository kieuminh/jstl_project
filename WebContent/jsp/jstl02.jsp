<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSTL 02</title>
</head>
<body>
	<h1>Core</h1>
	<h2>use List</h2>
	<div>ServerRequest > HttpSession > ServletContext
		<c:forEach var="elm" items="${list }">
			<c:if test="${elm.name == 'bloger 01' }">
				${elm.name }<br/><!-- .getName() -->
			</c:if>
			<!--  if -else  -->
			<c:choose>
				<c:when test="${elm.name == 'bloger 02' }">
				</c:when>
				<c:otherwise>
					${elm.name }<br/>
				</c:otherwise>
			</c:choose>
		</c:forEach>	
	</div>
	<div>	for<br/>
		<c:forEach var="i" begin="10" end="20" step="1" 
					varStatus="loop">
			var i = ${i } - ${loop.count }<br/>
		</c:forEach>
	</div>
	<a href="jstl01.jsp">JSTL 01</a>
	<a href="jstl03.jsp">JSTL 03</a>
</body>
</html>