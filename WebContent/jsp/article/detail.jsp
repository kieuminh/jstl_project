<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.coeding.servlet.vo.ArticleVO" %>
<!DOCTYPE html>
	<html>
<head>
<meta charset="UTF-8">
<title>View:Article</title>
</head>
<body>
<%
	ArticleVO article = (ArticleVO)request.getAttribute("article");
%>
	<section>
		<article>
			<h2><%=article.getTitle() %></h2>
			<p><%=article.getName() %>( <%=article.getWdate() %>)</p>
			<div>
				<%=article.getContent() %>
			</div>
		</article>
	</section>
</body>
</html>