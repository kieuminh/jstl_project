<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@ page import="com.coeding.servlet.vo.ArticleVO" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit:Article</title>
</head>
<body>
<%
	ArticleVO article = (ArticleVO)request.getAttribute("Article");
%>
	<div>
		<form action="edit" method="POST">
			<label>ArticleID</label>
            <input type="text" name="id" value="<%=article.getAid()%>">
			<label>Title</label>
            <input type="text" name="title" value="<%=article.getTitle()%>" >
			<label>Name</label>
            <input type="text" name="name" value="<%=article.getName()%>">
            <label>Content</label>
			<textarea name="content" cols="80" row="24">
				<%=article.getContent()%>
			</textarea>
			<input type="submit" value="SEND">
		</form>

		</form>
	</div>


</body>
</html>