<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSTL 01</title>
</head>
<body>
	<h1>Core</h1>
	<h2>variables, conditional expression, loop ... </h2>
	<div>
		<c:set var="uid" value="hongmari" scope="page"/>
		<c:if test="${ uid == 'hongmari' }">
			<p>True</p>" " and ' ' is string 
		</c:if>
		javascript EL use ' '<br/>
		HTML user " "<br/>
		switch(choose-when), if-else(when-otherwise)
		<c:choose>
			<c:when test="${login }">
				<p>Welcome</p>				
			</c:when>
			<c:otherwise>
				<p>please login</p>
			</c:otherwise>
		</c:choose>
		<c:set var="score" value="70" scope="page"/>
		<c:choose>
			<c:when test="${score >= 90 }">
				<p>High score</p>				
			</c:when>
			<c:when test="${score >= 70 }">
				<p>Middle score</p>				
			</c:when>
			<c:when test="${score >= 50 }">
				<p>Low score</p>				
			</c:when>
		</c:choose>
		
		
	</div>
	<div>
		declaration variables<br/>
		<c:set var="varName" value="hongmari" scope="page"/>
		<ul> scope
			<li>page</li>
			<li>request</li>
			<li>session</li>
			<li>application ( is ServletContext)</li>
		</ul>
		${varName }<br/>
		<c:remove var="varName"/>
		<c:out value="${varName }"></c:out>
		<c:out value="value to print"></c:out>
	</div>
	<a href="jstl02.jsp">JSTL 02</a>
</body>
</html>