<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.coeding.servlet.vo.BloggerVO" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Recv Attribute</title>
</head>
<body>

	<%--	// whe use Scriptlet
		BloggerVO bloger01 = (BloggerVO)request.getAttribute("bloger1");
		out.println( bloger01.getName() );
		BloggerVO bloger02 = (BloggerVO)request.getSession().getAttribute("bloger2");
		out.println( bloger02.getName() );
		BloggerVO bloger03 = (BloggerVO)request.getServletContext().getAttribute("bloger3");
		out.println( bloger03.getName() );
	--%>
	
	<p>EL ( Expression Language ) : auto searching <br/>
		ServletRequest -> HttpSession -> ServletContext
	</p>
	<p>${bloger1.name }</p>
	<p>${bloger2.name }</p>
	<p>${bloger3.name }</p>

	need loop with JSTL ( JSP Standard Tag Library )
	<p>${blogerList }</p>
	
</body>
</html>