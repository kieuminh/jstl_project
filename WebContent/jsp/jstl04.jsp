<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSTL 04</title>
</head>
<body>
	<h1>Fn</h1>
	<h2>String method</h2>
	<div>
		<c:set var="title" value="Huan's Rose shop"/>
		${title }<br/>
		${fn:trim(title) }<br/>
		
		<c:if test="${fn:length(title) > 10}">
			 ${fn:substring(title,0,10) }...<br/>
			  <!-- title.substring(0,10) -->
		</c:if>
		
		${fn:toUpperCase(title) }<br/>
		${fn:toLowerCase(title) }<br/>
		
		<c:if test="${fn:contains(title,'shop') }">
			is Shop<br/>
		</c:if>

		<c:if test="${title == 'shop' }">
			is Shop<br/>
		</c:if>
	</div>
	<a href="jstl01.jsp">JSTL 01</a>
</body>
</html>