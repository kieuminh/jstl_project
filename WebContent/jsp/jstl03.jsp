<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSTL 03</title>
</head>
<body>
	<h1>Fmt</h1>
	<h2>import java.util.Date</h2>
	<p>money, date, time have to be formatted</p>
	
	<div>
		<c:set var="now" value="<%=new Date() %>"/>
		<p>${now }</p>
		<fmt:timeZone value="Asia/Hanoi">
			<fmt:formatDate value="${now }" type="date" dateStyle="full"/><br/>
			<fmt:formatDate value="${now }" type="time" timeStyle="full"/><br/>
		</fmt:timeZone>
		<hr/>
		<fmt:formatDate value="${now }" type="date" dateStyle="full"/><br/>
		<fmt:formatDate value="${now }" type="date" dateStyle="short"/><br/>
		<fmt:formatDate value="${now }" type="time" timeStyle="full"/><br/>
		<fmt:formatDate value="${now }" type="time" timeStyle="short"/><br/>
		
		<fmt:formatDate value="${now }" pattern="MM-dd/YYYY" /><br/>
		<fmt:formatDate value="${now }" pattern="HH:mm:ss" /><br/>
		<!--  GMT or UTC(recommended) -->
		<!-- hh means am,pm 12 hours -->
		<!-- HH means 24 hours -->
		
		<c:set var="price" value="100000"/>
		<p>	${price } <br/>
			<fmt:formatNumber var="numPrice" value="${price }" type="number" />
			${numPrice }<br/>
			<fmt:formatNumber var="percentPrice" value="${price }" type="percent" />
			${percentPrice }<br/><!--  like excel's function -->
			<fmt:formatNumber var="currencyPrice" value="${price }" 
				currencySymbol="đ"
				type="currency" />
			${currencyPrice }<br/>
		</p>
	</div>
	<a href="jstl01.jsp">JSTL 01</a>
	<a href="jstl04.jsp">JSTL 04</a>
</body>
</html>