<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Upload file</title>
</head>
<body>
	<p>/WebContent/jsp/form-file.jsp</p>
	<p>to /WebContent/upload</p>
	
	<form action="../upload" method="post" enctype="multipart/form-data">
		<input type="file" name="fname">
		<input type="submit" value="upload">
	</form>
	<p>Expression Language & JSTLibrary</p>
	<img src="${filepath }">
	
</body>
</html>