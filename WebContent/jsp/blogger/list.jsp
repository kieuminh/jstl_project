<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.coeding.servlet.vo.BloggerVO" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List:Blogger</title>
</head>
<body>
	<h1>Java Server Page(JSP)</h1>
	<h2>java code + html</h2>

	<% // scriptlet JSP tag
		List<BloggerVO> list = 
		(List<BloggerVO>)request.getAttribute("userlist");
	%>
	<div style="border: 1px solid #ff0000">
		<table>
			<tr><td>No</td><td>Name</td><td>Emal</td><td>PW</td></tr>
		<% for(BloggerVO vo : list){ %>
			<tr>
				<td><%=vo.getUid() %></td>
				<td><%=vo.getName() %></td>
				<td><%=vo.getEmail() %></td>
				<td><%=vo.getPasswd() %></td>
			</tr>
		<% } %>
		</table>
	</div>

</body>
</html>