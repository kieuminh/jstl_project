<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Expression Language</title>
</head>
<body>
    <p>isELIgnored="false" default value </p>
    <p>has operator is not HTML</p>
	    == null
    <p>${"" == null}</p>
    <p>${empty ""}</p>
    <p>${"" != null}</p>
    <p>${not empty ""}</p>
    <p></p>
    
    <p>${ (10 eq 10) and (4 < 10) }</p>
    <p>${ (10 eq 10) or (4 < 10) }</p>
    <p>${ not (4 < 10) }</p>

    <p>${ (10 eq 10) && (4 < 10) }</p>
    <p>${ (10 eq 10) || (4 < 10) }</p>
    <p>${ !(4 < 10) }</p>
    
    <p>${ 10 == 10}</p>
    <p>${ 10 eq 10}</p>
    <p>${ 10 != 10}</p>
<!--     <p>${ 10 ne 10}</p>
 -->    <p>${ 23 < 10}</p>
    <p>${ 23 lt 10}</p>
    <p>${ 23 > 10 }</p>
    <p>${ 23 gt 10}</p>
    <p>${ 23 <= 10}</p>
    <p>${ 23 le 10}</p>
    <p>${ 23 >= 10 }</p>
    <p>${ 23 ge 10}</p>
    
    
	<p>${10 + 10}</p>
	<p>${10 - 10}</p>
	<p>${10 * 10}</p>
	<p>${10 / 10}</p>
<!-- 	<p>${10 div 2}</p>		<div></div> -->
 	<p>${10 % 10}</p>
	<p>${10 mod 2}</p>
</body>
</html>