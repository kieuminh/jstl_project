# webServlet

## implement HttpServlet
+ url mapping : web.xml or @WebServlet
+ override doGet, doPost

## ServletConfig and ServletContext 
+ read param data at web.xml

## use Tomcat's Connection Pooling
1. add lib "tomcat-dbcp" in /WEB-INF/lib
2. fill DB info in {Tomcat_HOME}/context.xml
3. get connection at your code

## JSP : View
+ java code + html
+ compile as Servlet in Servlet Container

directive : <%@ %> configuration
comment : 	<%-- dont execute --%>
			<!-- dont show --> in HTML
declaration :<%! %> field, method
expression :<%= %> print value of variable, return
scriptlet : <% %>	java code

## forwarding
(/blogger) -> BloggerServlet 
-> data ->
(/jsp/blogger/list.jsp) -> list.jsp(as Servlet) 
-> client

1. response.sendRedirect() -> client -> URL access
+ so, change URL at browser
2. RequestDispatcher -> 
+ dont change URL

## for Article

### URL				Servlet					JSP
+/article/list		ArticleListServlet		/jsp/article/list.jsp
+/article/new		ArticleNewServlet		/jsp/article/form-new.jsp
+/article/edit?id=	ArticleEditServlet		/jsp/article/form-edit.jsp
+/article/view?id=	ArticleDetailServlet	/jsp/article/detail.jsp
coding


# second impact
do implements simple web site with Servlet and JSP(basic)
27.08:00 ~ 28.16:00

now, start ~
-5점 / hour




















